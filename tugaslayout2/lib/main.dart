import 'package:flutter/material.dart';

void main() {
  runApp(
      Profile()
  );
}

class Profile extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('Profil Ku'),
          leading: Icon(Icons.menu),
          actions:<Widget>[
            IconButton(icon: Icon(Icons.thumb_up)),
            IconButton(icon: Icon(Icons.thumb_down)),
          ],
        ),
        body: ListView(
          children: <Widget>[
            Container(
              child:Image(
                  image: AssetImage('assets/picture.PNG')
              ),
            ),
            Container(
              child:Text('I Putu Angga Kerta Leona Putra',
                style: TextStyle(
                  color: Colors.red,
                  fontSize: 25.0,
                  height: 2.0,
                    fontWeight: FontWeight.bold,
                ),
              ),
              alignment: Alignment.center,
            )
          ],
        ),
      ),
    );
  }
}
